package controleatleta;

import java.util.ArrayList;
import java.util.Date;
import java.util.*;

public class Atleta{
	
	private ArrayList <String> telefone;
	private Date datanascimento;
	private String nome;
	private Endereco endereco;
	private Double altura;
	private String cpf;	

	public Atleta(string nome){
		this.nome = nome;
		this.endereco = new Endereco();
	}
	
	void setNome(String nome){
		this.nome = nome;
	}
	String getNome(){
		return nome;
	}
	void setDataNascimento(Date datanascimento){
		this.datanascimento = datanascimento;
	}
	Date getDataNascimento(){
		return datanascimento;
	}
	void setEndereco(Endereco endereco){
		this.endereco = endereco;
	}
	Endereco getEndereco(){
		return endereco;
	}
	void setAltura(double altura){
		this.altura = altura;
	}
	double getAltura(){
		return altura;
	}
	void setCpf(String cpf){
		this.cpf = cpf;
	}
	String getCpf(){
		return cpf;
	}
}
